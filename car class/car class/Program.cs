﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace car_class
{
    class Car
    {
        private string color;
        private string brand;
        public Car(string _color, string _brand)
        {
            color = _color;
            brand = _brand;
        }
        public override string ToString()
        {
            return color + ", " + brand;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Car car = new Car("blue", "Ferrari");
            Console.WriteLine(car);
            Console.ReadKey();
        }
    }
}
