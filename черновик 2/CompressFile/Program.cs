﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace CompressFile
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] num = new string[5];
            for (int i = 0; i < num.Length; ++i)
            {
                num[i] = Console.ReadLine();
                if (num[i] == "q")
                {
                    break;
                }
            }

            Regex rg = new Regex(@"^((8|\+7)[\-\s]?)?(\(?\d{3}\)?[\-\s]?)?([\d\-\s]){7,10}$");
                //("^([a-zA-Z])+([a-zA-Z0-9_.]){1,30}@([a-zA-Z0-9])+([a-zA-Z0-9_-]){1,25}.[a-zA-Z]{2,3}$");

            for (int i = 0; i < num.Length; ++i)
            {
                if (rg.IsMatch(num[i]))
                {
                    Console.Write("Input is correct!");
                    Console.WriteLine();
                }
                else
                {
                    Console.Write("Input is not correct!");
                    Console.WriteLine();
                }
            }

            Console.ReadKey();
        }
    }
}
