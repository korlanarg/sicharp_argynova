﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace CompressionDemo
{
    class Program
    {
        static void CompressFile(string inFilename, string outFilename)
        {
            FileStream sourceFile = File.Open(inFilename, FileMode.Open);
            FileStream destFile = File.Open(outFilename, FileMode.Open);
            GZipStream compStream = new GZipStream(destFile, CompressionMode.Compress);
            int theByte = sourceFile.ReadByte();
            while (theByte != -1)
            {
                compStream.WriteByte((byte)theByte);
                theByte = sourceFile.ReadByte();
            }
            sourceFile.Close();
            destFile.Close();
            //compStream.Close();
        }
        static void UncompressFile(string inFilename, string outFilename)
        {
            FileStream sourceFile = File.Open(inFilename, FileMode.Open);
            FileStream destFile = File.Open(outFilename, FileMode.Open);
            GZipStream compStream = new GZipStream(sourceFile, CompressionMode.Decompress);
            int theByte = compStream.ReadByte();
            while (theByte != -1)
            {
                destFile.WriteByte((byte)theByte);
                theByte = compStream.ReadByte();
            }
            sourceFile.Close();
            destFile.Close();
            //compStream.Close();
        }

        static void Main(string[] args)
        {
            CompressFile(@"c:\boot.ini", @"c:\boot.ini.gz");
            UncompressFile(@"c:\boot.ini.gz", @"c:\boot.ini.test");
            Console.ReadKey();
        }
    }
}
