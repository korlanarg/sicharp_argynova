﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace @try
{
    public partial class Form1 : Form
    {
        public int a = 0;
        public int b = 0;
        public int c = 0;
        public int diskriminant = 0;
        int k = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            a = int.Parse(textBox1.Text);
            b = int.Parse(textBox2.Text);
            c = int.Parse(textBox3.Text);
            diskriminant = b * b - 4 * a * c;
            if (diskriminant > 0)
            {
                k = 2;
                Answer.Text += k;
            }
            else if (diskriminant == 0)
            {
                k = 1;
                Answer.Text += k;
            }
            else
            {
                k = 0;
                Answer.Text += k;
            }
        }
    }
}
