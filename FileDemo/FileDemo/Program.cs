﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamWriter writer = File.CreateText(@"c:\new_file.txt");
            writer.WriteLine("This is new file!");
            writer.WriteLine("There are too much classes");
            writer.Close();
            StreamReader reader = File.OpenText(@"c:\new_file.txt");
            string contents = reader.ReadToEnd();
            reader.Close();
            Console.WriteLine(contents);
            Console.ReadKey();
        }
    }
}
