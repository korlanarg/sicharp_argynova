﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace Phone_num_or_zip_code
{
    class Program
    {
        static bool IsPhone(string s)
        {
            return Regex.IsMatch(s, @"^\(?\d{3}\)?[\s\-]?\d{3}\-?\d{4}$");
        }

        static bool IsZip(string s)
        {
            return Regex.IsMatch(s, @"^\d{5}(\-\d{4})?$");
        }

        static string ReformatPhone(string s)
        {
            Match m = Regex.Match(s, @"^\(?(\d{3})\)?[\s\-]?(\d{3})\-?(\d{4})$");
            return String.Format("({0}) {1}-{2}", m.Groups[1], m.Groups[2], m.Groups[3]);
        }

        static void Main(string[] args)
        {
            string[] input = new string[8];
            for (int i = 0; i < input.Length; ++i)  // filling the array
            {
                input[i] = Console.ReadLine();
            }

            /*for (int i = 0; i < input.Length; ++i)                        // from ex. 1
            {
                if (IsPhone(input[i]))
                {
                    Console.WriteLine("{0} a phone number", input[i]);
                } 
                if (IsZip(input[i]))
                {
                    Console.WriteLine("{0} a zip code", input[i]);
                }
                else
                {
                    Console.WriteLine("{0} is unknown", input[i]);
                }
            }*/

            foreach (string s in input)                                                      // from ex. 2
            {
                if (IsPhone(s)) Console.WriteLine(ReformatPhone(s) + " is a phone number");
                else if (IsZip(s)) Console.WriteLine(s + " is a zip code");
                else Console.WriteLine(s + " is unknown");
            }

            Console.ReadKey();
        }
    }
}
