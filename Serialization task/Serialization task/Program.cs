﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

public enum Genders { Male, Female }

namespace Serialization_task
{
    public class Student
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public Genders gender { get; set; }
        [XmlIgnore]
        public int year_of_birth { get; set; }

        public void Save(string filename)
        {
            using (var stream = new FileStream(filename, FileMode.Create))
            {
                var XML = new XmlSerializer(typeof(Student));
                XML.Serialize(stream, this);
            }
        }
        public static Student LoadFromFile(string filename)
        {
            using (var stream = new FileStream(filename, FileMode.Open))
            {
                var XML = new XmlSerializer(typeof(Student));
                return (Student)XML.Deserialize(stream);
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Student student = new Student();
            student.Name = "Mark";
            student.Surname = "Zuckerberg";
            student.year_of_birth = 1984;
            student.Save("StudentInfo.xml");
            student.gender = Genders.Male;

            Console.ReadKey();
        }
    }
}
