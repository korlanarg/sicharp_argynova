﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileSystemWatcher_task
{
    class Program
    {
        static void Main(string[] args)
        {
            FileSystemWatcher watcher = new FileSystemWatcher();
            watcher.Filter = ".txt";
            watcher.Created += new FileSystemEventHandler(watcher_FileCreated);
            watcher.Path = @"c:\";
            watcher.EnableRaisingEvents = true;

            /*string[] stringArray = new string[]          // next task
	        { "cat", "dog", "arrow" };
	        File.WriteAllLines(@"c:\file.txt", stringArray);
            */

            Console.ReadKey();
        }

        static void watcher_FileCreated(object sender, FileSystemEventArgs e)
        {
            Console.WriteLine("A new *.txt file has been created.");
        }
    }
}
