﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

struct Dnumber
{
    public int a, b;
    public Dnumber(int _a, int _b)
    {
        a = _a;
        b = _b;
    }
    public override string ToString()
    {
        return a.ToString() + "/" + b.ToString();
    }

    public void normize()
    {
        int x = a;
        int y = b;
        while (x > 0 && y > 0)
        {
            if (x > y)
                x = x % y;
            else
                y = y % x;
        }
        x = x + y;
        a /= x;
        b /= x;
    }
    public static Dnumber operator *(Dnumber arg1, Dnumber arg2)
    {
        arg1.a *= arg2.b;
        arg1.b *= arg2.a;
        arg1.normize();
        return arg1;
    }
}

namespace Division
{
    class Program
    {
        static void Main(string[] args)
        {
            Dnumber x = new Dnumber(1, 3);
            Dnumber y = new Dnumber(5, 9);
            Dnumber z = x * y;
            Console.WriteLine(z);
            Console.ReadKey();
        }
    }
}
