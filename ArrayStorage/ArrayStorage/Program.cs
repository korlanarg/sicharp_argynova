﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

struct BigInteger
{
    public int size;
    public int[] arr;
    public BigInteger (string number)
    {
        arr = new int [1001];
        int j = 0;
        for (int i = number.Length - 1; i >= 0; --i)
        {
            arr[j] = number[i] - '0';
            j++;
        }
        size = number.Length;
    }

    public override string ToString ()
    {
        string _number = " ";
        for (int i = size - 1; i >= 0; --i)
        {
            _number += arr[i].ToString();
        }
        return _number;
    }

    public static BigInteger operator + (BigInteger arg1, BigInteger arg2)
    {
        int max_size;
        BigInteger res = new BigInteger("");
        if (arg1.size > arg2.size)
        {
            max_size = arg1.size;
        }
        else
        {
            max_size = arg2.size;
        }

        for (int i = 0; i < max_size; i++)
        {
            res.arr[i] = arg1.arr[i] + arg2.arr[i];
            if (res.arr[i] > 9)
            {
                res.arr[i] = res.arr[i] % 10;
                arg1.arr[i + 1] += 1;

                if (i == max_size - 1)
                    max_size++;
            }
            res.size++;
        }

        return res;
    }
}

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] b;
            Console.Write("Enter the amount of big numbers: ");
            int n = int.Parse(Console.ReadLine());
            b = new string[n];

            Console.Write("Enter big numbers: ");
            Console.WriteLine();
            for (int i = 0; i < n; ++i)
            {
                b[i] = Console.ReadLine();
            }

            BigInteger z = new BigInteger("0");

            for (int i = 0; i < n; ++i)
            {
                BigInteger x = new BigInteger(b[i]);
                z += x;
                //Console.WriteLine(z);
            }
            Console.WriteLine("The result: " + z);
            Console.ReadKey();
        }
    }
}
