﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int x = int.Parse(textBox1.Text);
            int y = int.Parse(textBox2.Text);
            Graphics g = this.CreateGraphics();
            Pen p = new Pen(Color.Black, 10);
            Brush b = new SolidBrush(Color.BlueViolet);
            //DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            g.DrawRectangle(p, x, y, 100, 100);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int x = int.Parse(textBox1.Text);
            int y = int.Parse(textBox2.Text);
            Graphics g = this.CreateGraphics();
            Pen p = new Pen(Color.Black, 10);
            Brush b = new SolidBrush(Color.BlueViolet);
            g.DrawEllipse(p, x, y, 100, 100);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int x = int.Parse(textBox1.Text);
            int y = int.Parse(textBox2.Text);
            Graphics g = this.CreateGraphics();
            Pen p = new Pen(Color.Black, 10);
            g.DrawLine(p, x, y, x + 100, y + 100);
        }

        
    }
}
