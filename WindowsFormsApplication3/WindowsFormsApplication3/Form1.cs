﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {
        enum Position {Left, Right}
        private int _x;
        private int _y;
        private Position obj_position;

        public Form1()
        {
            InitializeComponent();
            _x = 325;
            _y = 200;
            obj_position = Position.Right;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (obj_position == Position.Right)
            {
                _x += 10;
            }
            Invalidate();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (obj_position == Position.Right)
            {
                _x -= 10;
            }
            Invalidate();
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.FillRectangle(Brushes.CadetBlue, _x, _y, 100, 20); // ordinary rectangle
            e.Graphics.DrawImage(new Bitmap("Pinkyyghost.png"), _x, _y, 64, 64); // image
        }
    }
}
