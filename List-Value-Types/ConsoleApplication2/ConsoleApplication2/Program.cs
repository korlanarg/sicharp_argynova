﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum Genders : int { Male, Female };

struct Person
{
    public string firstName;
    public string lastName;
    public int age;
    public Genders gender;
    public Person(string _firstName, string _lastName, int _age, Genders _gender)
    {
        firstName = _firstName;
        lastName = _lastName;
        age = _age;
        gender = _gender;
    }
    public override string ToString()
    {
        return firstName + " " + lastName + " (" + gender + "), age " + age;
    }
}

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            Person a = new Person("Helen", "Adams", 21, Genders.Female);
            Console.WriteLine(a.ToString());
            Console.ReadLine();
        }
    }
}
