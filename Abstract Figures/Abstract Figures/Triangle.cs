﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Figures
{
    class Triangle : Figure
    {
        public double a;
        public double b;
        public double c;
        public Triangle(double a, double b, double c)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        public override double perimeter()
        {
            return a + b + c;
        }
        public override double area()
        {
            double p = (a + b + c) / 2;
            return Math.Sqrt(p * (p - a) * (p - b) * (p - c));
        }
        public override string nameFigure()
        {
            return "This is triangle!";
        }
    }
}
