﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Figures
{
    class Rectangle : Figure
    {
        double a;
        double b;
        public Rectangle(double a, double b)
        {
            this.a = a;
            this.b = b;
        }
        public override double perimeter()
        {
            return 2 * (a + b);
        }
        public override double area()
        {
            return a * b;
        }
        public override string nameFigure()
        {
            return "This is rectangle!";
        }
    }
}
