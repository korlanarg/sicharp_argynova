﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Figures
{
    class Circle : Figure
    {
        double radius;
        public Circle(double r)
        {
            radius = r;
        }
        public override double perimeter()
        {
            return 2 * Math.PI * radius; // 2*pi*r
        }
        public override double area()
        {
            return Math.PI * radius * radius; // pi*r^2
        }
        public override string nameFigure()
        {
            return "This is circle!";
        }
    }
}
