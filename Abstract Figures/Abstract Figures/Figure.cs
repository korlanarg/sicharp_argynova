﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Figures
{
    abstract class Figure
    {
        public abstract string nameFigure();
        public abstract double perimeter();
        public abstract double area();
    }
}
