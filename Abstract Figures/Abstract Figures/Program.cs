﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstract_Figures
{
    class Program
    {
        static void Main(string[] args)
        {
            double a = 3;
            double b = 4;
            double c = 5;
            Figure triangle = new Triangle(a, b, c);
            Figure circle = new Circle(b);
            Figure rectangle = new Rectangle(a, b);

            Console.WriteLine(triangle.perimeter());
            Console.WriteLine(triangle.area());
            Console.WriteLine(triangle.nameFigure());

            Console.WriteLine(triangle.perimeter());
            Console.WriteLine(triangle.area());
            Console.WriteLine(triangle.nameFigure());

            Console.WriteLine(triangle.perimeter());
            Console.WriteLine(triangle.area());
            Console.WriteLine(triangle.nameFigure());
            Console.ReadKey();
        }
    }
}
