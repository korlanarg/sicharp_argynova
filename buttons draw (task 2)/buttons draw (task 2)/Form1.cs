﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace buttons_draw__task_2_
{
    public partial class Form1 : Form
    {
        MyButton[,] btns = new MyButton[6, 6];
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    btns[i, j] = new MyButton(i, j);
                    btns[i, j].Size = new Size(40, 40);
                    btns[i, j].Location = new Point(i * 45, j * 45);
                    btns[i, j].Click += new EventHandler(button1_Click);
                    this.Controls.Add(btns[i, j]);
                }
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            MyButton b = sender as MyButton;
            int x = b.x;
            int y = b.y;

            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    if ((x <= 0) || (y <= 0) || (x >= 6) || (y >= 6))
                    {
                        continue;
                    }
                    btns[x, y].BackColor = Color.Red;
                    if (x == i)
                    {
                        btns[i, y - 1].BackColor = Color.Red;
                        btns[i, y + 1].BackColor = Color.Red;
                    }
                    else if (y == j)
                    {
                        btns[x - 1, j].BackColor = Color.Red;
                        btns[x + 1, j].BackColor = Color.Red;
                    }
                    else
                    {
                        btns[i, j].BackColor = Color.Black;
                    }
                }
            }
        }
    }
}
