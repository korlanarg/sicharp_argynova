﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace ConsoleApplication4
{
    class Point
    {
        public int x, y;
        public Point(int _x, int _y)
        {
            x = _x;
            y = _y;
        }

        public override string ToString()
        {
            return x + " " + y + "/n";
        }

        public static Point operator + (Point a, Point b)
        {
            Point c = new Point(a.x + b.x, a.y + b.y);
            return c;
        }
    }

    class Line
    {
        Point a;
        Point b;
        public Line(Point a, Point b)
        {
            this.a = a;
            this.b = b;
        }

        public double lineLength(Point a, Point b)
        {
            double c = (a.x - b.x) * (a.x - b.x);
            double d = (a.y - b.y) * (a.y - b.y);
            return Math.Sqrt(c + d);
        }
    }

    class Polygon
    {
        public List<Point> points = new List<Point>();

        public Polygon(List<Point> p)
        {
            p = points;
        }
        
        public double Perimeter()
        {
            double res = 0;
            for (int i = 1; i < s.Length; ++i)
            {
                double temp = Convert.ToDouble(new Line(points[i - 1], points[i]));
                res = res + temp;
            }
            double temp1 = Convert.ToDouble(new Line(points[0], points[points.Count - 1]));
            res += temp1;
            return res;
        }

        string s = "";
        public override string ToString()
        {
            foreach (Point p in points)
            {
                s = s + p + "/n";
            }
            return s;
        }

        public static double operator +(Polygon p1, Polygon p2) // perimeter
        {
            double sum = 0;
            sum += p1.Perimeter() + p2.Perimeter();
            return sum;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            StreamReader sr = new StreamReader(@"c:\polygon.txt");
            List<Point> list = new List<Point>();

            while (sr.Peek() != -1)
            {
                string [] s = sr.ReadLine().Split();
                int x = int.Parse(s[0]);
                int y = int.Parse(s[1]);
                Point p = new Point(x, y);
                list.Add(p);             
            }

            Polygon polygon = new Polygon(list);
            Console.WriteLine(polygon);
            //Console.WriteLine(polygon.Perimeter());
            
            sr.Close();
            Console.ReadKey();
        }
    }
}
