﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace StreamWriter_and_StreamReader_tasks
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader sr = new StreamReader(@"c:\new_file.txt");
            GZipStream gzOut = new GZipStream(File.OpenWrite(@"c:\output.txt"), CompressionMode.Compress);
            StreamWriter sw = new StreamWriter(gzOut);
            List<int> numbers = new List<int>();
             
            string[] s = sr.ReadLine().Split(' ');
            for (int i = 0; i < s.Length; ++i)
            {
                int temp = int.Parse(s[i]);
                numbers.Add(temp);
            }
            sr.Close();

            int sum = 0;
            foreach (int k in numbers) 
            {
                sum += k;
                sw.WriteLine(k);
            }
            sw.WriteLine(sum);

            sw.Close();
            gzOut.Close();

            GZipStream gzIn = new GZipStream(File.OpenRead(@"c:\output.txt"), CompressionMode.Decompress);
            StreamReader sr1 = new StreamReader(gzIn);
            //Console.WriteLine(sr1.ReadToEnd()); // that works

            sr1.Close();
            gzIn.Close();

            Console.ReadKey();
        }
    }
}
