﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Graphics g;
            g = this.CreateGraphics();
            Pen p = new Pen(Color.Gold, 50);
            Brush b = new SolidBrush(Color.Blue);
            g.FillRectangle(b, 20, 20, 400, 250);
            g.DrawLine(p, 20, 140, 420, 140);
            g.DrawLine(p, 155, 20, 155, 270);

        }
    }
}
