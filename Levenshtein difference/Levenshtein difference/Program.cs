﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace Levenshtein_difference
{
    static class LevenshteinAlgorithm
    {
        public static int Count(string x, string y)
        {
            int n = x.Length;
            int m = y.Length;
            int[,] D = new int[n + 1, m + 1];

            for (int i = 0; i <= n; ++i) // initialization
            {
                for (int j = 0; j <= m; ++j)
                {
                    D[i, 0] = i;
                    D[0, j] = j;
                }
            }

            int edit;

            for (int i = 0; i < n; ++i)     // min number of single-charactered edits required to
            {                               // change one word to another
                for (int j = 0; j < m; ++j)
                {
                    if (x[i] != y[j])
                    {
                        edit = 1;
                    }
                    else
                    {
                        edit = 0;
                    }
                    D[i + 1, j + 1] = Math.Min(D[i, j + 1] + 1, Math.Min(D[i + 1, j] + 1, D[i, j] + edit));
                }
            }
            return D[n, m];
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            string x;
            Console.Write("Enter the string: ");
            x = Console.ReadLine();

            StreamReader sw = new StreamReader(@"c:\Users\Astana 2017\Desktop\words.txt");

            while (sw != null)
            {
                string y = sw.ReadLine();
                int z = Math.Max(x.Length, y.Length);

                int a = LevenshteinAlgorithm.Count(x, y);

                int percent = 100 * a / z;
                if (percent <= 30)
                {
                    Console.WriteLine(y);
                }
             }

            Console.ReadKey();
        }
    }
}
