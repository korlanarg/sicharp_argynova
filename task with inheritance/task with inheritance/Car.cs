﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_with_inheritance
{
    class Car : Vehicle
    {
        public override string coordinates(double x, double y)
        {
            return string.Format("Coordinates of a car: {0}, {1}", x, y);
        }
        public override string parameters(double price, double velocity, int year)
        {
            return string.Format("Parameters of a car: {0}, {1}, {2}", price, velocity, year);
        }
    }
}
