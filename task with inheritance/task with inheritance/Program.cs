﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_with_inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            Ship myship = new Ship();
            myship.numPassengers = 300;
            Console.WriteLine(myship.parameters(200000, 50, 2014));
            Console.WriteLine(myship.coordinates(32.00, 53.1));
            Console.WriteLine(myship.home_port());
            Console.WriteLine(myship.numPassengers);

            Vehicle mycar = new Car();
            Console.WriteLine(mycar.parameters(35000, 150, 2013));
            Console.WriteLine(mycar.coordinates(61.8, 15.6));

            Plane plane = new Plane();
            plane.Height = 300;
            plane.numPassengers = 120;
            Console.WriteLine(plane.parameters(1000000, 350, 2008));
            Console.WriteLine(plane.coordinates(150, 300));
            Console.WriteLine(plane.Height);
            Console.WriteLine(plane.numPassengers);

            Console.ReadKey();
        }
    }
}
