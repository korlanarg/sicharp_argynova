﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_with_inheritance
{
    abstract class Vehicle
    {
        public virtual string coordinates(double x, double y)
        {
            string result = string.Format("Coordinates: {0}, {1}", x, y);
            return result;
        }
        public abstract string parameters(double price, double velocity, int year);
    }
}
