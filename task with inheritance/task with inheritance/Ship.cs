﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task_with_inheritance
{
    class Ship : Vehicle
    {
        public override string coordinates(double x, double y)
        {
            return string.Format("Coordinates of a ship: {0}, {1}", x, y);
        }
        public override string parameters(double price, double velocity, int year)
        {
            return string.Format("Parameters of a ship: {0}, {1}, {2}", price, velocity, year);
        }
        public int numPassengers { get; set; }
        public string home_port()
        {
            return "Crimea, Russian Federation";
        }
    }
}
